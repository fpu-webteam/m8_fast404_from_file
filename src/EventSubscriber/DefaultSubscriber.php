<?php

namespace Drupal\m8_fast404_from_file\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\EventSubscriber\HttpExceptionSubscriberBase;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Provides the same functionality of Fast404ExceptionHtmlSubscriber, but uses
 * an HTML file instead of a string of HTML.
 *
 * @package Drupal\m8_fast404_from_file
 */
class DefaultSubscriber extends HttpExceptionSubscriberBase {

  /**
   * The HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, HttpKernelInterface $http_kernel) {
    $this->configFactory = $config_factory;
    $this->httpKernel = $http_kernel;
  }
  /**
   * {@inheritdoc}
   */
  protected static function getPriority() {
    // 1 higher than Fast404ExceptionHtmlSubscriber::getPriority().
    return 201;
  }

  /**
   * {@inheritdoc}
   */
  protected function getHandledFormats() {
    return ['html'];
  }

  /**
   * Handles a 404 error for HTML.
   *
   * @param \Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent $event
   *   The event to process.
   */
  public function on404(GetResponseForExceptionEvent $event) {
    $request = $event->getRequest();

    $config = $this->configFactory->get('system.performance');
    $exclude_paths = $config->get('fast_404.exclude_paths');
    if ($config->get('fast_404.enabled') && $exclude_paths && !preg_match($exclude_paths, $request->getPathInfo())) {
      $fast_paths = $config->get('fast_404.paths');
      if ($fast_paths && preg_match($fast_paths, $request->getPathInfo())) {
        $fast_404_html = strtr(file_get_contents(DRUPAL_ROOT . '/404.html'), ['@path' => Html::escape($request->getUri())]);
        $response = new Response($fast_404_html, Response::HTTP_NOT_FOUND);
        $event->setResponse($response);
      }
    }
  }
}
